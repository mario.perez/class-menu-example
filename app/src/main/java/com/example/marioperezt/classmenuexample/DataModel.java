package com.example.marioperezt.classmenuexample;

/**
 * Created by marioperezt on 10/28/17.
 */

public class DataModel {

    public int icon;
    public String name;

    // Constructor.
    public DataModel(int icon, String name) {

        this.icon = icon;
        this.name = name;
    }
}
