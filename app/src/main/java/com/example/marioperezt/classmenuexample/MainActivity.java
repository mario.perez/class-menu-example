package com.example.marioperezt.classmenuexample;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_demo, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_home:
                Toast.makeText(this, "Home",
                        Toast.LENGTH_LONG).show();
                return true;
            case R.id.action_help:
                Toast.makeText(this, "Help",
                        Toast.LENGTH_LONG).show();
                return true;
            case R.id.action_logout:
                Toast.makeText(this, "Bye Bye!!",
                        Toast.LENGTH_LONG).show();
                return true;
            case R.id.action_search:
                Intent intent = new Intent(this, NavigationActivity.class);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
